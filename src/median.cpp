#include <bits/stdc++.h>
#include <string>
#include <vector>
#include "median.h"


MedianCalc::MedianCalc() {
     Values.reserve(MAX_VALUES) ; // Initialize
    }

bool MedianCalc::AddValue(int Number) {
    if (Values.size() < MAX_VALUES)
       {
        Values.push_back(Number);
        return true;
       }
    else
       return false;

}

double MedianCalc::CalcMedian(void) {
    // Check for Odd size case
    int vsize =  Values.size(); 
    if (vsize % 2 != 0)
       {
        std::nth_element(Values.begin(), Values.begin() + vsize/2, Values.end());
        return (double) Values[vsize/2];
       }
    //Even size case
    std::nth_element(Values.begin(), Values.begin()+(vsize/2)-1, Values.end());
    int FirstMed =  Values[(vsize/2)-1];
    std::nth_element(Values.begin(), Values.begin() + (vsize/2), Values.end());
    return (double)( FirstMed + Values[(vsize/2)] )/2.0;
}

void MedianCalc::PrintSample(void) {
    if(Values.empty())
        { 
         std::cout << "<<No Values>>" << std::endl;
         return;
        }
    else
        {
         for ( int i = 0; i < (int)Values.size(); i++)
             {
              std::cout << Values[i] << " ";
              if ((i != 0) && ( (i+1) % 20 == 0)) std::cout << std::endl; // Print 20 values per line
             }
         std::cout << std::endl;
	 return;
        }
}

void MedianCalc::ResetVal() {
     if (!Values.empty()) Values.clear();
}


// Class for simple sort Median calculation
MedianSimple::MedianSimple() {
     ResetVal(); // Initialize
    }

bool MedianSimple::AddValue(int Number) {
    if (NextVal<MAX_VALUES)
       {
        Values[NextVal++] = Number;
        return true;
       }
    else
       return false;

}

double MedianSimple::CalcMedian(void) {
    std::sort(Values, Values+NextVal);

    // check for odd number case
    if (NextVal % 2 != 0)
       return (double)Values[NextVal/2];
    //even number case
    return (double)(Values[(NextVal/2)-1] + Values[NextVal/2])/2.0;
}

void MedianSimple::PrintSample(void) {
    if(NextVal == 0)
        {
         std::cout << "<<No Values>>" << std::endl;
         return;
        }
    else
        {
         for (int i = 0; i < NextVal; i++)
             {
              std::cout << Values[i] << " ";
              if ((i != 0) && ( (i+1) % 20 == 0)) std::cout << std::endl; // Print 20 values per line
             }
         std::cout << std::endl;
	 return;
        }
}

void MedianSimple::ResetVal() {
     NextVal = 0; //Init Next value index - also count of values
}
