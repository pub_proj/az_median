#include <iostream>
#include <string>
#include <bits/stdc++.h>
#include "median.h"
using namespace std;

bool DoubleComp(double a, double b, double eps = 0.001)
{
    return std::abs(a - b) < eps;
}

int main() {
   int FailedTests = 0;
   int DoneTests = 0;


    MedianCalc mcalc;
    MedianSimple ctrl;
    /**
     * Test 01 Fixed test with odd number of values from Wikipedia
    */
    cout << "==================== Fixed Test 01 ============" << endl;
    cout << "Initial Sample:" << endl;
    mcalc.PrintSample();
    //Add values to sample
    mcalc.AddValue(1);
    mcalc.AddValue(3);
    mcalc.AddValue(3);
    mcalc.AddValue(6);
    mcalc.AddValue(7);
    mcalc.AddValue(8);
    mcalc.AddValue(9);
    //Print sample
    cout << "Sample after adding values:"  << endl;
    mcalc.PrintSample();
    double Result = mcalc.CalcMedian();
    cout << "Median: " << Result << endl;
    if (!DoubleComp(Result, 6))
        {
         FailedTests++;
         cout << "TEST FAILED!" << endl;
        }
    else
        cout << "TEST OK" << endl;
    DoneTests++;

    /**
     * Test 02 Fixed test with even number of values from Wikipedia
    */
    cout << "==================== Fixed Test 02 ============" << endl;
    mcalc.ResetVal();
    cout << "Initial Sample:" << endl;
    mcalc.PrintSample();
    //Add values to sample
    mcalc.AddValue(1);
    mcalc.AddValue(2);
    mcalc.AddValue(3);
    mcalc.AddValue(4);
    mcalc.AddValue(5);
    mcalc.AddValue(6);
    mcalc.AddValue(8);
    mcalc.AddValue(9);
    //Print sample
    cout << "Sample after adding values:"  << endl;
    mcalc.PrintSample();
    Result = mcalc.CalcMedian(); 
    cout << "Median: " << Result << endl;
    if (!DoubleComp(Result, 4.5))
        {
         FailedTests++;
         cout << "TEST FAILED!" << endl;
        }
    else
        cout << "TEST OK" << endl;
    DoneTests++;

    /**
     * Test 03 Consecutive numbers in reverse order test with easily
     * predictable result
     * to verify Simple Median calculation with non-optimized sort
    */
    cout << "==================== Fixed Test 03 ============" << endl;
    ctrl.ResetVal();
    cout << "Initial Sample:" << endl;
    ctrl.PrintSample();
    //Add values to sample ( 19 to 1)
    for (int v = 19; v>0; v--) ctrl.AddValue(v);
    //Print sample
    cout << "Sample after adding values:"  << endl;
    ctrl.PrintSample();
    Result = ctrl.CalcMedian();
    cout << "Median: " << Result << endl;
    if (!DoubleComp(Result, 10))
        {
         FailedTests++;
         cout << "TEST FAILED!" << endl;
        }
    else
        cout << "TEST OK" << endl;
    DoneTests++;

    /**
     * Test 04 Consecutive numbers in a range(odd total count)
     * in non sorted order - test with easily predictable result
     * to verify Simple Median calculation with non-optimized sort
     * Example: 5 numbers, Mid is 3: 5,1,4,2,3
    */
    cout << "==================== Fixed Test 04 ============" << endl;
    ctrl.ResetVal();
    cout << "Initial Sample:" << endl;
    ctrl.PrintSample();
    //Add values to sample ( range 1 to 19)
    int Mid = 10;
    int Tot = 19;
    int LoopMax = Tot/2; // 9

    for (int v = LoopMax; v>0; v--)
        {
          ctrl.AddValue(Mid+v);
          ctrl.AddValue(Mid-v);
        }
    //Ad the middle one
    ctrl.AddValue(Mid);

    //Print sample
    cout << "Sample after adding values:"  << endl;
    ctrl.PrintSample();
    Result = ctrl.CalcMedian();
    cout << "Median: " << Result << endl;
    if (!DoubleComp(Result, Mid))
        {
         FailedTests++;
         cout << "TEST FAILED!" << endl;
        }
    else
        cout << "TEST OK" << endl;
    DoneTests++;

    /**
     * Test 05 Consecutive numbers in a range(even total count)
     * in non sorted order - test with easily predictable result
     * to verify Simple Median calculation with non-optimized sort
     * Example: 6 numbers, 6,1,5,2,4,3
    */
    cout << "==================== Fixed Test 05 ============" << endl;
    ctrl.ResetVal();
    cout << "Initial Sample:" << endl;
    ctrl.PrintSample();
    //Add values to sample ( range 1 to 19)
    Tot = 40;
    LoopMax = Tot/2; // 20

    for (int v = LoopMax; v>0; v--)
        {
          ctrl.AddValue(v);
          ctrl.AddValue(Tot-v+1);
        }

    //Print sample
    cout << "Sample after adding values:"  << endl;
    ctrl.PrintSample();
    Result = ctrl.CalcMedian();
    cout << "Median: " << Result << endl;
    if (!DoubleComp(Result, (Tot+1)/2.0))
        {
         FailedTests++;
         cout << "TEST FAILED!" << endl;
        }
    else
        cout << "TEST OK" << endl;
    DoneTests++;

 /**
     * Test 07 Random numbers in a range - not sorted
     * use MedianCalc class to calulate and MedinSimple class
     * to verify the result.
     * 
    */
    cout << "====================  Random Test 07 ===========" << endl;
    ctrl.ResetVal();
    mcalc.ResetVal();
    cout << "Initial Sample Median:" << endl;
    mcalc.PrintSample();
    cout << "Initial Sample Median:" << endl;
    ctrl.PrintSample();
    //Add values to sample ( Total 60 values, Max = 100)
    Tot = 60;
    int NumMax = 100;
    // Random Seeed
    //Note: Use of rand() is deprecated in C++14, but for the sake of simplicity in this simple test is probably acceptable
    srand(time(NULL)); 

    for (int i = 0; i < Tot; i++)
        {
          int Tmp = rand() % NumMax;
          mcalc.AddValue(Tmp);
          ctrl.AddValue(Tmp);
        }

    //Print samples
    cout << "Median Sample after adding values:"  << endl;
    mcalc.PrintSample();
    cout << "Control Sample after adding values:"  << endl;
    ctrl.PrintSample();

    Result = mcalc.CalcMedian();
    cout << "Median Result: " << Result << endl;
    double ControlResult = ctrl.CalcMedian();
    cout << "Control Result: " << ControlResult << endl << endl;

    //Print sorted samples
    cout << "Median Sample after sort/select:"  << endl;
    mcalc.PrintSample();
    cout << "Control Sample after sort/select:"  << endl;
    ctrl.PrintSample();

    if (!DoubleComp(Result, ControlResult))
        {
         FailedTests++;
         cout << "TEST FAILED!" << endl;
        }
    else
        cout << "TEST OK" << endl;
    DoneTests++;

    //Display  Test Results
    cout << "===============================================" << endl;
    cout << "** Summary **********" << endl; 
    cout << "** All Tests   : " << DoneTests << endl;
    cout << "** FAILED Tests: " << FailedTests << endl;
    cout << "** End of tests *****" << endl;
    cout << "=============================" << endl;

  return 0;
}
