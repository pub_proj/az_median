#include <bits/stdc++.h>
#include <string>
#define  MAX_VALUES 256
/**
 ** Class used for Median Calculation with optimized method
*/
class MedianCalc {
  public:              
    MedianCalc();
    bool AddValue(int Number);
    double CalcMedian(void); 
    void PrintSample(void);
    void ResetVal();
 private:
    std::vector<int> Values;
};

/**
 ** Class used for Median Calculation as a reference in dynamic tests
 ** with random numbers. Using non-ptimized sort method, but has easy
 ** to predict result in order to verify optimized methods.
*/
class MedianSimple {
  public:
    MedianSimple();
    bool AddValue(int Number);
    double CalcMedian(void);
    void PrintSample(void);
    void ResetVal();
 private:
    int Values[MAX_VALUES];
    int NextVal;
};
