# AZ_Median - Median calculation and tests

**Building**

*  **Clone repository in a local directory**
*  *cd az_median*
*  *make debug* **or** *make release*
*  **if build is successfull run:** *./build/apps/median*
*  *make clean* to remove /build tree ( object files and executables)

**Testing**

There is no **tests** target in Makefile - main program is used for tests
Two types of tests are used:

* Fixed tests - input values and results are pre-generated manually. These tests do not require reference code to verify the results of the code tested, but takes more time to generate and verify fixed values and write test code. Test writers need to be very competent on the tested matter and to be able to write test cases for all interesting options in a limited set, or alternativery use a lot of time to write manually all possible cases.
* Dymanic tests - input values are generated randomly or with some specific code and results are verified using reference code/program. The advantage is that these can be more complete/exhaustive tests, but need anther pice of code that is already verified and has the same functionality. This could be useful if the same code is translated in another programming language, ported to another platform etc.

**Classes**

*  *MedianCalc* - class with optimized calculation using internal select function  **std::nth_element()**
*  *MedianSimple* - class with non-optimal calculation used to verify the *MedianCalc* class operation in dynamic tests with random numbers

**Internal structures**

* Plain array is used to store values in **MedianSimple** class and Vector for **MedianCalc** class
* Integer values are assumed and **int** type is used (32 bits on most architectures)
* MAX_VALUES constant is used to limit values to a reasonable number
